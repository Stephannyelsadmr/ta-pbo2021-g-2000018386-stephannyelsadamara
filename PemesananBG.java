package com.company;

import java.util.Scanner;

public class PemesananBG {
    String Menu="";
    int Pilihan, JumlahBunga,Harga;
    int Total=0;
    Scanner scan = new Scanner(System.in);
    Scanner input = new Scanner(System.in);
    String nama, def="Masukkan ";

    void Menu(){
        System.out.println("             ------------------------------           ");
        System.out.println("             PEMESANAN BUNGA STELDA FLORIST           ");
        System.out.println("             ------------------------------           ");

        System.out.print(def + "Nama \t\t: ");
        nama = input.nextLine();

        System.out.println("                  Daftar Harga Bunga                  ");

        System.out.println("NO  Jenis Bunga                HARGA BUNGA");
        System.out.println("1.  BUCKET MAWAR                 Rp 200.000.-");
        System.out.println("2.  BUCKET MATAHARI              Rp 210.000.-");
        System.out.println("3.  BUCKET ANGGREK               RP 250.000.-");
        System.out.println("4.  KARANGAN BUNGA SIZE S        RP 300.000.-");
        System.out.println("5.  KARANGAN BUNGA SIZE M        RP 450.000.-");
        System.out.println("6.  KARANGAN BUNGA SIZE L        RP 600.000.-");
        System.out.println("7.  KARANGAN BUNGA SIZE XL       RP 750.000.-");
        System.out.println("-----------------------------");
        System.out.println("Melakukan Pemesanan Bunga ");
        System.out.println("Masukan Nomor Pesanan Yang Ingin Anda Pesan :");
        Pilihan = scan.nextInt();
        switch (Pilihan){
            //untuk delivery, biaya layanan 10% dibayarkan saat COD
            case 1:
                System.out.println("| Nama\t\t: " + nama);
                System.out.println("Pesanan Anda BUCKET MAWAR       Rp 200.000.-"); //Belum termasuk biaya layanan
                System.out.print("Masukan Jumlah Bunga Yang Ingin Anda Pesan :");
                JumlahBunga=scan.nextInt();
                Harga= 200000 *JumlahBunga;
                System.out.println("Harga pesanan = Rp. "+Harga);
                System.out.println("Total pembayaran Rp "+Harga+" untuk "+JumlahBunga+" Bucket Mawar");
                Total=Total+Harga;
                System.out.println("Silahkan melakukan pembayaran cash di kasir (TAKE AWAY) atau COD (DELIVERY) ");
                System.out.println("                -TERIMA KASIH-               ");
                break;
            case 2:
                System.out.println("| Nama\t\t: " +nama);
                System.out.println("Pesanan Anda BUCKET MATAHARI          Rp 210.000.- ");
                System.out.print("Masukan Jumlah Bunga Yang Ingin Anda Pesan :");
                JumlahBunga=scan.nextInt();
                Harga= 210000 *JumlahBunga;
                System.out.println("Harga pesanan = Rp. "+Harga);
                System.out.println("Total pembayaran Rp "+Harga+" untuk "+JumlahBunga+" Bucket Matahari");
                Total=Total+Harga;
                System.out.println("Silahkan melakukan pembayaran cash di kasir (TAKE AWAY) atau COD (DELIVERY) ");
                System.out.println("                -TERIMA KASIH-               ");
                break;
            case 3:
                System.out.println("| Nama\t\t: " +nama);
                System.out.println("Pesanan Anda BUCKET ANGGREK            RP 250.000.-");
                System.out.print("Masukan Jumlah Pesanan Yang Ingin Anda Pesan :");
                JumlahBunga=scan.nextInt();
                Harga= 170000 *JumlahBunga;
                System.out.println("Harga pesanan = Rp. "+Harga);
                System.out.println("Total pembayaran Rp "+Harga+" untuk "+JumlahBunga+" Bucket Anggrek");
                Total=Total+Harga;
                System.out.println("Silahkan melakukan pembayaran cash di kasir (TAKE AWAY) atau COD (DELIVERY) ");
                System.out.println("                -TERIMA KASIH-               ");
                break;
            case 4:
                System.out.println("| Nama\t\t: " +nama);
                System.out.println("Pesanan Anda KARANGAN BUNGA SIZE S        RP 300.000.-");
                System.out.print("Masukan Jumlah Tiket Yang Ingin Anda Pesan :");
                JumlahBunga=scan.nextInt();
                Harga= 300000 *JumlahBunga;
                System.out.println("Harga pesanan = Rp. "+Harga);
                System.out.println("Total pembayaran Rp "+Harga+" untuk "+JumlahBunga+" Karangan Bunga Size S");
                Total=Total+Harga;
                System.out.println("Silahkan melakukan pembayaran cash di kasir (TAKE AWAY) atau COD (DELIVERY) ");
                System.out.println("                -TERIMA KASIH-               ");
                break;
            case 5:
                System.out.println("| Nama\t\t: " +nama);
                System.out.println("Pesanan Anda KARANGAN BUNGA SIZE M         RP 450.000.-");
                System.out.print("Masukan Jumlah Tiket Yang Ingin Anda Pesan :");
                JumlahBunga=scan.nextInt();
                Harga= 450000 *JumlahBunga;
                System.out.println("Harga pesanan = Rp. "+Harga);
                System.out.println("Total pembayaran Rp "+Harga+" untuk "+JumlahBunga+" Karangan Bunga Size M");
                Total=Total+Harga;
                System.out.println("Silahkan melakukan pembayaran cash di kasir (TAKE AWAY) atau COD (DELIVERY) ");
                System.out.println("                -TERIMA KASIH-               ");
                break;
            case 6:
                System.out.println("| Nama\t\t: " +nama);
                System.out.println("Pesanan Anda KARANGAN BUNGA SIZE L         RP 600.000.-");
                System.out.print("Masukan Jumlah Tiket Yang Ingin Anda Pesan :");
                JumlahBunga=scan.nextInt();
                Harga= 600000 *JumlahBunga;
                System.out.println("Harga pesanan = Rp. "+Harga);
                System.out.println("Total pembayaran Rp "+Harga+" untuk "+JumlahBunga+" Karangan Bunga Size L");
                Total=Total+Harga;
                System.out.println("Silahkan melakukan pembayaran cash di kasir (TAKE AWAY) atau COD (DELIVERY) ");
                System.out.println("                -TERIMA KASIH-               ");
                break;
            case 7:
                System.out.println("| Nama\t\t: " +nama);
                System.out.println("Pesanan Anda KARANGAN BUNGA SIZE XL       RP 750.000.-");
                System.out.print("Masukan Jumlah Tiket Yang Ingin Anda Pesan :");
                JumlahBunga=scan.nextInt();
                Harga= 750000 *JumlahBunga;
                System.out.println("Harga pesanan = Rp. "+Harga);
                System.out.println("Total pembayaran Rp "+Harga+" untuk "+JumlahBunga+" Karangan Bunga Size XL");
                Total=Total+Harga;
                System.out.println("Silahkan melakukan pembayaran cash di kasir (TAKE AWAY) atau COD (DELIVERY) ");
                System.out.println("                -TERIMA KASIH-               ");
                break;

        }
    }
}